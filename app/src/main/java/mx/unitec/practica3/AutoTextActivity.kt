package mx.unitec.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.Toast
import java.lang.reflect.Array

class AutoTextActivity : AppCompatActivity() {

    lateinit var autotextView: AutoCompleteTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auto_text)

        autotextView = findViewById(R.id.autoCompleteTextViewAlcaldia)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.alcaldia_array,
            android.R.layout.select_dialog_item

        )

        autotextView.threshold = 2

        autotextView.setAdapter(adapter)

        autotextView.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this,
            position.toString() + ": " + parent?. getItemAtPosition(position).toString(),
            Toast.LENGTH_SHORT).show()
        }
    }
}